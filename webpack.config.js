const path = require('path');
const getLocalIdent = require('css-loader/lib/getLocalIdent');
const nodeExternals = require('webpack-node-externals');

module.exports = {
   mode: 'production',
   entry: path.resolve(__dirname, 'lib', 'index.js'),
   output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'index.js',
      library: 'hatch-content-atomics',
      libraryTarget: 'umd',      
      umdNamedDefine: true  
   },
   target: 'node', // in order to ignore built-in modules like path, fs, etc.
   externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
   resolve: {
      extensions: [".js", ".jsx"]
   },
   // Enable sourcemaps for debugging webpack's output.
   devtool: 'source-map',
   module: {
      rules: [
         {
             test: /\.(js|jsx)$/,
             exclude: /(node_modules|bower_components)/,
             use: {
                loader: 'babel-loader'
             }
         },
         {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          use: {
             loader: 'eslint-loader'
          }
        },          
        {
            test: /\.css$/,
            exclude: /(node_modules|bower_components)/,
            use: [
                require.resolve('style-loader'),
                {
                    loader: 'typings-for-css-modules-loader',
                    options: {
                        namedExport: true,
                        camelCase: true,
                        modules: true,
                        localIdentName: '[name]--[local]--[hash:base64:5]',
                        minimize: false,
                        importLoaders: 1,
                        sourceMap: true,
                        getLocalIdent: (context, localIdentName, localName, options) => {
                            return getLocalIdent(context, localIdentName, localName, options).toLocaleLowerCase();
                        },
                    },
                },
            ],
        },
        {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
                {
                    loader: 'url-loader',
                    options:{
                        fallback: 'file-loader',
                        name: '[name][md5:hash].[ext]',
                        outputPath: path.resolve(__dirname, 'static'),
                    }
                }
            ]
        },
      ]
   }
};