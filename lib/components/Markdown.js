import React from "react";
import { Editor, EditorState, CompositeDecorator, RichUtils, convertToRaw, convertFromRaw } from "draft-js";
import PropTypes from "prop-types";
import ReferenceComponent from "./ReferenceComponent";
import { Attachment } from "@spacehatch/hatch-content-atomics";

// Regex {NUMBER}
const REFERENCE_REGEX = /{[\d]+}/g;

function handleReferenceStrategy(contentBlock, callback, contentState) {
  const text = contentBlock.getText();
  let matchArr = REFERENCE_REGEX.exec(text);
  let start;
  while (matchArr !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
    matchArr = REFERENCE_REGEX.exec(text);
  }
}

// Regex LINK
function findLinkEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === 'LINK'
      );
    },
    callback
  );
}

const Link = (state) => {
  const { url } = state.contentState.getEntity(state.entityKey).getData();
  return (
    <a
      href={url}
      target="_blank"
    >
      {state.children}
    </a>
  );
};

class Markdown extends React.Component {

  constructor(props) {
    super(props);

    const attachmentsDecorator = {
      component: ReferenceComponent,
      props: { attachments: this.props.attachments },
      strategy: handleReferenceStrategy,
    };
    const linkDecorator = {
      component: Link,
      strategy: findLinkEntities,
    };

    // Set state with text provided
    if ( this.props.text && this.props.text.length > 0 ) {
      this.state = {
        editorState: EditorState.createWithContent(
          convertFromRaw(JSON.parse((this.props.text))),
          new CompositeDecorator([attachmentsDecorator, linkDecorator]),
        ),
      };
    } else {
      this.state = {
        editorState: EditorState.createEmpty(
          new CompositeDecorator([attachmentsDecorator, linkDecorator]),
        ),
      };      
    }

    // Bind
    this.handleChange = this._handleChange.bind(this);
    this.handleBoldClick = this._handleBoldClick.bind(this);
    this.handleItalicClick = this._handleItalicClick.bind(this);
    this.handleBulletPointsClick = this._handleBulletPointsClick.bind(this);
    this.handleSubtitleClick = this._handleSubtitleClick.bind(this);
    this.handleLinkClick = this._handleLinkClick.bind(this);
  }

  _handleChange(editorState) {
    // Sync editor with record text
    const rawText = JSON.stringify(convertToRaw(editorState.getCurrentContent()));
    this.props.onChange(rawText);
    this.setState({ editorState });
  };

  /**
   * Examples: https://github.com/facebook/draft-js/blob/master/examples/draft-0-10-0/rich/rich.html
   */
  _handleBoldClick(e) {
    e.preventDefault();
    this.handleChange(RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"));
  }

  _handleItalicClick(e) {
    e.preventDefault();
    this.handleChange(RichUtils.toggleInlineStyle(this.state.editorState, "ITALIC"));
  }

  _handleBulletPointsClick(e) {
    e.preventDefault();
    this.handleChange(RichUtils.toggleBlockType(this.state.editorState, "unordered-list-item"));
  }

  _handleSubtitleClick(e) {
    e.preventDefault();
    this.handleChange(RichUtils.toggleBlockType(this.state.editorState, "header-three"));
  }

  _handleLinkClick(e) {
    e.preventDefault();
    const { editorState } = this.state;
    const urlValue = prompt("Please enter your link", "http://");
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity("LINK", "MUTABLE", { url: urlValue });
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    this.handleChange(RichUtils.toggleLink(newEditorState, newEditorState.getSelection(), entityKey));
  }

  render() {
    return (
      <div>
        {!this.props.readOnly && (
          <div style={{ marginTop: "10px", marginBottom: "10px" }}>
            <button onClick={this.handleBoldClick}><b>Bold</b></button>
            <button onClick={this.handleItalicClick}><i>Italic</i></button>
            <button onClick={this.handleBulletPointsClick}>&#8226;</button>
            <button onClick={this.handleSubtitleClick}>H3</button>
            <button onClick={this.handleLinkClick}>Link</button>
          </div>
        )}
        <Editor
          editorState={this.state.editorState}
          onChange={this.handleChange}
          readOnly={this.props.readOnly}
        />
      </div>
    );
  }
}

Markdown.propTypes = {
  attachments: PropTypes.arrayOf(Attachment),
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
  text: PropTypes.string.isRequired,
};

Markdown.defaultProps = {
  onChange: () => {},
  readOnly: false
};

export default Markdown;
