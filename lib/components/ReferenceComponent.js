import React from "react";
import PropTypes from "prop-types";
import { QuoteText, QuoteMedia, Gallery, Attachment, Video } from "@spacehatch/hatch-content-atomics";

class ReferenceComponent extends React.Component {

  constructor(props) {
    super(props);

    const { attachments, decoratedText } = props;
    const attachmentNumber = decoratedText.match(/\d+/)[0];
    const attachment = attachments ? attachments.find((it) => parseInt(it.frontPageOrder, 10) === parseInt(attachmentNumber, 10)) : undefined;

    this.state = {
      attachment: attachment,
      number: attachmentNumber,
    };
  }

  render() {
    if ( this.state.attachment ) {
      return (
        <div>
          {this.state.attachment.attachmentType === "quoteText" &&
            <QuoteText attachment={this.state.attachment.quoteText} />
          }
          {this.state.attachment.attachmentType === "quoteMedia" &&
            <QuoteMedia attachment={this.state.attachment.quoteMedia} />
          }
          {this.state.attachment.attachmentType === "gallery" &&
            <Gallery attachment={this.state.attachment.gallery} />
          }
          {this.state.attachment.attachmentType === "video" &&
            <Video attachment={this.state.attachment.video} />
          }
        </div>
      );
    } else {
      return (
        this.props.children
      );
    }
  }
}

ReferenceComponent.propTypes = {
  attachments: PropTypes.arrayOf(Attachment),
  children: PropTypes.node.isRequired,
  decoratedText: PropTypes.string.isRequired,
};

export default ReferenceComponent;
