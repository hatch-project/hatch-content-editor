# hatch-content-editor

#### Development
```
npm run build:watch // build package automatically from lib/ to build/
npm run lint:watch // check automatically for lint errors
npm link // link package for local development
npm link @spacehatch/hatch-content-atomics // use package linked with previous step
npm publish --access public
```