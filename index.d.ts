// Type definitions for hatch-content-editor > v0.1.1
// Project: https://bitbucket.org/hatch-project/hatch-content-editor

import * as React from "react";

declare class Markdown extends React.Component<Markdown.Props, any> {}

declare namespace Markdown {

  export interface Props {
    text: string
    attachments: Array<{
      attachmentType: string,
      quoteText?: {
          leftText: string,
          rightText: string,
      },
      quoteMedia?: {
          text: string,
          media: string,
          webLink: string,
          source?: string,
          sourceLink?: string,
      },
      gallery?: Array<{
          media: string,
          webLink: string,
          source?: string,
          sourceLink?: string,
      }>,
    }>
    onChange?: (text: string) => void
    readOnly?: boolean
  }
}